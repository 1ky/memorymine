import java.util.Random;

public class Amplium extends ReactionAgent
{
    private final int MINREACT = 2; // constant that holds minimum value for ore
    private final int MAXREACT = 3; // constant that holds maximum value for ore

    /** Method  accepts ore  and  adds  the  reactionFactor  to  the
     * value  of  the  ore before returning it. */
    public Ore oreReaction(Ore ore)
    {
        if (ore instanceof Simploxide)
        {
            ((Simploxide) ore).setValue(((Simploxide) ore).getValue() * getReactionFactor());
        }
        return ore;
    } // oreReaction

    /** Constructor used to initialize all data members.
     * Note: symbol  for Amplium is ‘X’; the value of the
     * reactionFactor is  a random number in the range 2-3. */
    public Amplium()
    {
        setType("Amplium");
        setSymbol('X');

        Random rand = new Random();
        setReactionFactor(rand.nextInt(MAXREACT-MINREACT)+MINREACT);
    } // Amplium
} // Amplium
