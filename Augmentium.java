import java.util.Random;

public class Augmentium extends ReactionAgent
{
    private final int MINREACT = 2; // constant that holds minimum value for ore
    private final int MAXREACT = 4; // constant that holds maximum value for ore

    /** Constructor used to initialize all data members.
     * Note:symbol for Augmentium is ‘A’; the value of the reactionFactor
     * is a random number in the range 2-4. */
    public Augmentium()
    {
        setType("Augmentium");
        setSymbol('A');

        Random rand = new Random();
        setReactionFactor(rand.nextInt(MAXREACT-MINREACT)+MINREACT);
    } // Augmentium
} // Augmentium
