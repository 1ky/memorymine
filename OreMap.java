import java.util.Random;

public class OreMap
{
    private int rows; // stores the max number of rows on the map
    private int columns; // stores the max number of columns on the map
    private int simploxideCount; // tracks the amount of simploxide ore left on the map
    private int augmentiumCount; // tracks the amount of augmentium ore left on the map
    private int minuciumCount; // tracks the amount of minucium ore left on the map
    private int ampliumCount; // tracks the amount of amplium ore left on the map
    private int shiftoxideCount; // tracks the amount of shiftoxide ore left on the map [max = 4]
    private int redoxineCount;  // tracks the amount of redoxine ore left on the map [max = 4]
    private Ore map[][]; // 2d array which is filled randomly with ore

    /** Getter for rows field*/
    public int getRows()
    {
        return rows;
    } // getRows

    /** Setter for rows field*/
    public void setRows(int rows)
    {
        this.rows = rows;
    } // setRows

    /** Getter for columns field*/
    public int getColumns()
    {
        return columns;
    } // getColumns

    /** Setter for columns field*/
    public void setColumns(int columns) {
        this.columns = columns;
    } // setColumns

    /** Getter for simploxideCount field*/
    public int getSimploxideCount()
    {
        return simploxideCount;
    } // getSimploxideCount

    /** Setter for simploxideCount field*/
    public void setSimploxideCount(int simploxideCount)
    {
        this.simploxideCount = simploxideCount;
    } // setSimploxideCount

    /** Getter for augmentiumCount field*/
    public int getAugmentiumCount()
    {
        return augmentiumCount;
    } // getAugmentiumCount

    /** Setter for augmentiumCount field*/
    public void setAugmentiumCount(int augmentiumCount)
    {
        this.augmentiumCount = augmentiumCount;
    } // setAugmentiumCount

    /** Getter for minuciumCount field*/
    public int getMinuciumCount() {
        return minuciumCount;
    } // getMinuciumCount

    /** Setter for minuciumCount field*/
    public void setMinuciumCount(int minuciumCount)
    {
        this.minuciumCount = minuciumCount;
    } // setMinuciumCount

    /** Getter for ampliumCount field*/
    public int getAmpliumCount()
    {
        return ampliumCount;
    } // getAmpliumCount

    /** Setter for ampliumCount field*/
    public void setAmpliumCount(int ampliumCount)
    {
        this.ampliumCount = ampliumCount;
    } // setAmpliumCount

    public int getShiftoxideCount() {
        return shiftoxideCount;
    }

    public void setShiftoxideCount(int shiftoxideCount) {
        this.shiftoxideCount = shiftoxideCount;
    }

    public int getRedoxineCount() {
        return redoxineCount;
    }

    public void setRedoxineCount(int redoxineCount) {
        this.redoxineCount = redoxineCount;
    }

    public Ore getMapPosition(int r, int c)
    {
        return map[r][c];
    }

    public void setMapPosition(int r, int c, Ore ore)
    {
        map[r][c] = ore;
    }

    /** Used to randomly fill the map with all types of ore.
     * No positions in the map are left empty. */
    public void fillMap()
    {
        Random rand = new Random();

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                switch (rand.nextInt(6))
                {
                    case 1:
                        setMapPosition(i,j,new Augmentium());
                        setAugmentiumCount(getAugmentiumCount()+1);
                        break;
                    case 2:
                        setMapPosition(i,j,new Minucium());
                        setMinuciumCount(getMinuciumCount()+1);
                        break;
                    case 3:
                        setMapPosition(i,j,new Amplium());
                        setAmpliumCount(getAmpliumCount()+1);
                        break;
                    case 4:
                        if (getShiftoxideCount() >= 4)
                        {
                            j -= 1;
                            continue;
                        }
                        setMapPosition(i,j,new Shiftoxide());
                        setShiftoxideCount(getShiftoxideCount()+1);
                        break;
                    case 5:
                        if (getRedoxineCount() >= 4)
                        {
                            j -= 1;
                            continue;
                        }
                        setMapPosition(i,j,new Redoxine());
                        setRedoxineCount(getRedoxineCount()+1);
                        break;
                    default:
                        setMapPosition(i,j,new Simploxide());
                        setSimploxideCount(getSimploxideCount()+1);
                        break;
                }
            }

        }
    } // fillMap

    /** Displays a summary of the ore currently in the mine */
    public void displayOreSummary()
    {
        System.out.println("\n\nOre Remaining");
        System.out.printf("Simploxide: %d\nAugmentium: %d\nMinucium: %d\nAmplium: %d\nShiftoxide: %d\nRedoxine: %d\n",
                getSimploxideCount(), getAugmentiumCount(), getMinuciumCount(), getAmpliumCount(), getShiftoxideCount(),
                getRedoxineCount());
    } // displayOreSummary

    /** Prints the map using a “hidden” format and then calls displayOreSummary() */
    public void displayMapAsHidden()
    {
        for (int i = 0; i < getRows()+1; i++)
        {
            switch (i)
            {
                case 0:
                    System.out.printf("\n   |");
                    break;
                default:
                    System.out.printf("\n %d |",i-1);
                    break;
            }
            for (int j = 0; j < getColumns(); j++)
            {
                switch (i)
                {
                    case 0:
                        System.out.printf(" %d |", j);
                        break;
                    default:
                        if (getMapPosition(i-1,j) == null)
                        {
                            System.out.printf(" - |");
                        }
                        else
                        {
                            System.out.printf("   |");
                        }
                        break;
                }
            }
        }
        displayOreSummary();
    } // displayMapAsHidden

    /** Prints the map using a “visible” format and then calls displayOreSummary() */
    public void displayMapAsVisible()
    {
        for (int i = 0; i < getRows()+1; i++)
        {
            switch (i)
            {
                case 0:
                    System.out.printf("\n   |");
                    break;
                default:
                    System.out.printf("\n %d |",i-1);
                    break;
            }
            for (int j = 0; j < getColumns(); j++)
            {
                switch (i)
                {
                    case 0:
                        System.out.printf(" %d |", j);
                        break;
                    default:
                        if (getMapPosition(i-1,j) == null)
                        {
                            System.out.printf(" - |");
                        }
                        else
                        {
                            System.out.printf(" %c |", getMapPosition(i-1,j).getSymbol());
                        }
                        break;
                }
            }
        }
        displayOreSummary();
    } // displayMapAsVisible

    /** Displays map with only selected positions,
     * [row1][col1] and [row2][col2] as visible. */
    public void showOre(int row1, int col1, int row2, int col2)
    {
        for (int i = 0; i < getRows()+1; i++)
        {
            switch (i)
            {
                case 0:
                    System.out.printf("\n   |");
                    break;
                default:
                    System.out.printf("\n %d |",i-1);
                    break;
            }
            for (int j = 0; j < getColumns(); j++)
            {
                switch (i)
                {
                    case 0:
                        System.out.printf(" %d |", j);
                        break;
                    default:
                        if ((i-1 == row1 && j == col1) || (i-1 == row2 && j == col2))
                        {
                            if (getMapPosition(i-1,j) == null)
                            {
                                System.out.print(" - |");
                            }
                            else
                            {
                                System.out.printf(" %c |", getMapPosition(i - 1, j).getSymbol());
                            }
                        }
                        else
                        {
                            System.out.printf("   |");
                        }
                        break;
                }
            }
        }
        displayOreSummary();
    } // showOre

    /** Similar to showOre method but displays all adjacent map positions
     * in addition to those that were selection by the player. This method is
     * only called under specific conditions. */
    public void displayMineAsSpecial(int row1, int col1, int row2, int col2)
    {
        for (int i = 0; i < getRows()+1; i++)
        {
            switch (i)
            {
                case 0:
                    System.out.print("\n   |");
                    break;
                default:
                    System.out.printf("\n %d |",i-1);
                    break;
            }
            for (int j = 0; j < getColumns(); j++)
            {
                switch (i)
                {
                    case 0:
                        System.out.printf(" %d |", j);
                        break;
                    default:
                        if ((i-1 == row1 && j == col1) || (i-1 == row2 && j == col2) ||
                                (i-1 == row1-1 && j == col1-1) || (i-1 == row2-1 && j == col2-1) ||
                                (i-1 == row1-1 && j == col1) || (i-1 == row2-1 && j == col2) ||
                                (i-1 == row1-1 && j == col1+1) || (i-1 == row2-1 && j == col2+1) ||
                                (i-1 == row1 && j == col1-1) || (i-1 == row2 && j == col2-1) ||
                                (i-1 == row1 && j == col1+1) || (i-1 == row2 && j == col2+1) ||
                                (i-1 == row1+1 && j == col1-1) || (i-1 == row2+1 && j == col2-1) ||
                                (i-1 == row1+1 && j == col1) || (i-1 == row2+1 && j == col2) ||
                                (i-1 == row1+1 && j == col1+1) || (i-1 == row2+1 && j == col2+1))
                        {
                            if (getMapPosition(i-1,j) == null)
                            {
                                System.out.print(" - |");
                            }
                            else
                            {
                                System.out.printf(" %c |", getMapPosition(i - 1, j).getSymbol());
                            }
                        }
                        else
                        {
                            System.out.print("   |");
                        }
                        break;
                }
            }
        }
        displayOreSummary();
    } // displayMineAsSpecial()

    /** Returns true if the ore at position
     * [row1][col1] in the mine matches the type of ore at position
     * [row2][col2]. Otherwise, it returns false. */
    public boolean checkOreMatch(int row1, int col1, int row2, int col2)
    {
        if (getMapPosition(row1,col1) != null && getMapPosition(row2,col2) != null)
        {
            return getMapPosition(row1, col1).getType() == getMapPosition(row2, col2).getType();
        }
        return false;
    } // checkOreMatch

    /** Checks if ore at position [r][c] in the map is Shiftoxide ore */
    public boolean isShiftoxide(int r, int c)
    {
        return "Shiftoxide".equals(getMapPosition(r, c).getType());
    } // isShiftoxide

    /** Shifts row in map based on the direction value of Shiftoxide ore
     * 0 - row is shifted left
     * 1 - row is shifted right. */
    public void shiftOre(Shiftoxide shifter, int row) throws OffBoardException
    {
        if (shifter.getDirection() == 0)
        {
            if (getMapPosition(row, 0) != null)
            {
                throw new OffBoardException(getMapPosition(row,0).getType());
            }

            for (int j = 0; j < getColumns(); j++)
            {
                if (j == getColumns()-1)
                {
                    setMapPosition(row, j, null);
                }
                else
                {
                    setMapPosition(row,j,getMapPosition(row,j+1));
                }
            }
        }
        else
        {
            if (getMapPosition(row, getColumns()-1) != null)
            {
                throw new OffBoardException(getMapPosition(row,getColumns()-1).getType());
            }

            for (int j = getColumns()-1; j >= 0; j--)
            {
                if (j == 0)
                {
                    setMapPosition(row,j,null);
                }
                else
                {
                    setMapPosition(row,j,getMapPosition(row,j-1));
                }
            }
        }
    } // shiftOre


    /** Checks if value at [r][c] is an instance of Redoxine */
    public boolean isRedoxine(int r, int c)
    {
        return "Redoxine".equals(getMapPosition(r, c).getType());
    } // isRedoxine

    /** Diminishes adjacent Ore in map based on the stability of Redoxine ore
     * stable - nothing happens
     * unstable - adjacent ore in map is diminished*/
    public void diminishOre(int row, int col)
    {
        if (row < 0)
        {
            row = 0;
        }
        else if (row > getColumns()-1)
        {
            row = getColumns()-1;
        }

        if (col < 0)
        {
            col = 0;
        }
        else if (col > getColumns()-1)
        {
            col = getColumns()-1;
        }


        if (getMapPosition(row,col) instanceof Redoxine)
        {
            if (!((Redoxine) getMapPosition(row, col)).getStable())
            {
                for (int i = -1; i < 2; i++)
                {
                    if (row + i >= 0 && row + i < getRows())
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            if (col + j >= 0 && col + j < getColumns())
                            {
                                if (!(getMapPosition(row + i, col + j) instanceof Redoxine))
                                {
                                    setMapPosition(row + i, col + j, null);
                                }
                            }
                        }
                    }
                }
            }
        }
    } // diminishOre

    /** Returns the ore located at position r, c in the mine. */
    public Ore mineOre(int r, int c)
    {
        if (getMapPosition(r,c) != null)
        {
            return getMapPosition(r, c);
        }
        else
        {
            return new Ore();
        }
    } // mineOre

    /** Used to remove the ore at position r, c in the mine. */
    public void removeOre(int r, int c)
    {
        switch (getMapPosition(r,c).getType())
        {
            case "Simploxide":
                setSimploxideCount(getSimploxideCount()-1);
                break;
            case "Augmentium":
                setAugmentiumCount(getAugmentiumCount()-1);
                break;
            case "Minucium":
                setMinuciumCount(getMinuciumCount()-1);
                break;
            case "Amplium":
                setAmpliumCount(getAmpliumCount()-1);
                break;
            case "Shiftoxide":
                setShiftoxideCount(getShiftoxideCount()-1);
                break;
            case "Redoxine":
                setRedoxineCount(getRedoxineCount()-1);
                break;
        }
        if (getMapPosition(r,c) != null)
        {
            setMapPosition(r, c, null);
        }
    } // removeOre

    /** Initializes the rows and cols fields to default 8; instantiates map
     * calls fillMap()*/
    public OreMap()
    {
        setRows(8);
        setColumns(8);
        setSimploxideCount(0);
        setAugmentiumCount(0);
        setMinuciumCount(0);
        setAmpliumCount(0);
        setShiftoxideCount(0);
        setRedoxineCount(0);
        map = new Ore[getRows()][getColumns()];

        fillMap();
    } // OreMap

    /** Overloaded Constructor of OreMap
     * Initializes the rows and cols fields to parameters r and c; instantiates map
     * calls fillMap()*/
    public OreMap(int r, int c)
    {
        setRows(r);
        setColumns(c);
        setSimploxideCount(0);
        setAugmentiumCount(0);
        setMinuciumCount(0);
        setAmpliumCount(0);
        setShiftoxideCount(0);
        setRedoxineCount(0);
        map = new Ore[getRows()][getColumns()];

        fillMap();
    } // OreMap
} // OreMap
