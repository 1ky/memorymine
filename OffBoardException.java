public class OffBoardException extends Exception
{
    /** Overridden toString() Method */
    public String toString()
    {
        return "Falling off Board";
    } // OffBoardException

    /** Constructor for OffBoardException exception class */
    public OffBoardException(String s)
    {
        super(s);
    } // OffBoardException
} // OffBoardException
