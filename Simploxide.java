import java.util.Random;

public class Simploxide extends Ore
{
    private int value; // stores the value of the ore
    private final int MINVALUE = 1; // constant that holds minimum value for ore
    private final int MAXVALUE = 3; // constant that holds maximum value for ore

    /** Getter for the value field */
    public int getValue()
    {
        return value;
    } // getValue

    /** Setter for the value field */
    public void setValue(int value)
    {
        this.value = value;
    } // setValue

    /** Constructor used to initialize all data members.
     * Note:symbol  for  Simploxide  is  ‘S’; the  value of  the  ore is
     * a random number in the range 1-3. */
    public Simploxide()
    {
        setType("Simploxide");
        setSymbol('S');

        Random rand = new Random();
        setValue(rand.nextInt(MAXVALUE-MINVALUE)+MINVALUE);
    } // Simploxide
} // Simploxide
