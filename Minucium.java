import java.util.Random;

public class Minucium extends ReactionAgent
{
    private final int MINREACT = 1; // constant that holds minimum value for ore
    private final int MAXREACT = 3; // constant that holds maximum value for ore

    /** Method  accepts ore  and  adds  the  reactionFactor  to  the
     * value  of  the  ore before returning it. */
    public Ore oreReaction(Ore ore)
    {
        if (ore instanceof Simploxide)
        {
            ((Simploxide) ore).setValue(((Simploxide) ore).getValue() - getReactionFactor());
        }
        return ore;
    } // oreReaction

    /** Constructor used to initialize all data members.
     * Note: symbol for Minucium is ‘M’; the value of
     * the reactionFactor is a random number in the range 1-3. */
    public Minucium()
    {
        setType("Minucium");
        setSymbol('M');

        Random rand = new Random();
        setReactionFactor(rand.nextInt(MAXREACT-MINREACT)+MINREACT);
    } // Minucium
} // Minucium
