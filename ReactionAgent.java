import java.util.Random;

public class ReactionAgent extends Ore
{
    private int reactionFactor; // holds the reaction factor of the reaction ore

    /** Getter for the reactionFactor field */
    public int getReactionFactor()
    {
        return reactionFactor;
    } // getReactionFactor

    /** Setter for the reactionFactor field */
    public void setReactionFactor(int reactionFactor)
    {
        this.reactionFactor = reactionFactor;
    } // setReactionFactor

    /** Method  accepts ore  and  adds  the  reactionFactor  to  the
     * value  of  the  ore before returning it. */
    public Ore oreReaction(Ore ore)
    {
        if (ore instanceof Simploxide)
        {
            ((Simploxide) ore).setValue(((Simploxide) ore).getValue() + getReactionFactor());
        }
        return ore;
    } // oreReaction

    /** Constructor used to initialize reactionFactor to 1. */
    public ReactionAgent()
    {
        setReactionFactor(1);
    } // ReactionAgent
} // ReactionAgent
