import java.util.Random;
import java.util.Scanner;

public class MemoryMine
{
    public static void main(String[] args)
    {
        // Welcomes player and gets their name
        System.out.println("Welcome to Memory Mine!");
        System.out.println("What is your name?");

        Scanner scan = new Scanner(System.in);
        String pname = scan.nextLine();


        Player player1 = new Player(pname);
        System.out.printf("Good luck in the Memory Mine, %s!\n\n", player1.getName());

        // Fills the map with ore randomly and displays it to the player
        OreMap map = new OreMap();
        map.displayMapAsVisible();

        // Loop runs until exit condition is met
        // or player decides to end the game
        while (map.getSimploxideCount() > 0)
        {
            // Generates a wildcard value between 1 and 7 inclusive
            Random roundRandom = new Random();
            int wildCard = roundRandom.nextInt(7)+1;

            // Displays the map (with ore values hidden) to the player
            // then receives values for corresponding map positions.
            // Invalid integer values are replaced with randomly generated values.
            map.displayMapAsHidden();
            System.out.println("\nValues which are out of bounds will be replaced with randomly generated values.");
            System.out.println("Enter your first row coordinate.");
            int xLocation1 = scan.nextInt();
            if (xLocation1 < 0 || xLocation1 > map.getRows()-1)
            {
                xLocation1 = roundRandom.nextInt(map.getRows());
            }

            System.out.println("Enter your first column coordinate.");
            int yLocation1 = scan.nextInt();
            if (yLocation1 < 0 || yLocation1 > map.getColumns()-1)
            {
                yLocation1 = roundRandom.nextInt(map.getColumns());
            }

            System.out.println("Enter your second row coordinate.");
            int xLocation2 = scan.nextInt();
            if (xLocation2 < 0 || xLocation2 > map.getRows()-1)
            {
                xLocation2 = roundRandom.nextInt(map.getRows());
            }

            System.out.println("Enter your second column coordinate.");
            int yLocation2 = scan.nextInt();
            if (yLocation2 < 0 || yLocation2 > map.getColumns()-1)
            {
                yLocation2 = roundRandom.nextInt(map.getColumns());
            }

            if (map.checkOreMatch(xLocation1,yLocation1,xLocation2,yLocation2))
            {
                if (wildCard == 3 || wildCard == 5) // Special case which determines if displayMineAsSpecial method is called
                {
                    map.displayMineAsSpecial(xLocation1, yLocation1, xLocation2, yLocation2);

                    if (map.getMapPosition(xLocation1,yLocation2).getType().equals("Redoxine"))
                    {
                        map.diminishOre(xLocation1,yLocation1);
                        map.diminishOre(xLocation2,yLocation2);
                    }

                    // i is initialized as -1 with a max value of 1
                    // It is added to the row values, allowing us to access adjacent rows
                    for (int i = -1; i < 2; i++)
                    {
                        // if i + selected row is less than 0 or exceeds the max row, no operations are done
                        if (xLocation1 + i >= 0 && xLocation1 + i < map.getRows())
                        {
                            // j is initialized as -1 with a max value of 1
                            // It is added to the column values, allowing us to access adjacent columns
                            for (int j = -1; j < 2; j++)
                            {
                                // if j + selected column is less than 0 or exceeds the max column, no operations are done
                                if (yLocation1 + j >= 0 && yLocation1 + j < map.getColumns())
                                {
                                    if (map.getMapPosition(xLocation1 + i, yLocation1 + j) != null)
                                    {
                                        switch (map.mineOre(xLocation1 + i, yLocation1 + j).getType())
                                        {
                                            case "Simploxide":
                                                player1.storeOre(map.mineOre(xLocation1 + i, yLocation1 + j));
                                                map.removeOre(xLocation1 + i, yLocation1 + j);
                                                break;
                                            case "Augmentium":
                                            case "Minucium":
                                            case "Amplium":
                                                player1.storeReactor(map.mineOre(xLocation1 + i, yLocation1 + j));
                                                map.removeOre(xLocation1 + i, yLocation1 + j);
                                                break;
                                            case "Shiftoxide":
                                                int adjustment = 0;
                                                if (i == 0 && j == 0) // shiftOre only called if positions match the coordinates chosen by user
                                                {
                                                    try
                                                    {
                                                        int shift = ((Shiftoxide) map.getMapPosition(xLocation1 + i, yLocation1 + j)).getDirection();
                                                        map.shiftOre(((Shiftoxide) map.getMapPosition(xLocation1 + i, yLocation1 + j)), xLocation1 + i);
                                                        adjustment = (shift == 0) ? -1 : 1;
                                                    }
                                                    catch (OffBoardException e)
                                                    {
                                                        System.out.printf("\n* On Row %d, %s\n", xLocation1 + i, e.toString());
                                                    }
                                                }
                                                map.removeOre(xLocation1 + i, yLocation1 + j + adjustment);
                                                break;
                                            case "Redoxine":
                                                if (i == 0 && j == 0) // diminishOre only called if positions match the coordinates chosen by user
                                                {
                                                    map.diminishOre(xLocation1 + i, yLocation1 + i);
                                                }
                                                map.removeOre(xLocation1 + i, yLocation1 + j);
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (int i = -1; i < 2; i++)
                    {
                        if (xLocation2 + i >= 0 && xLocation2 + i < map.getRows())
                        {
                            for (int j = -1; j < 2; j++)
                            {
                                if (yLocation2 + j >= 0 && yLocation2 + j < map.getColumns()) {
                                    if (map.getMapPosition(xLocation2 + i, yLocation2 + j) != null)
                                    {
                                        switch (map.mineOre(xLocation2 + i, yLocation2 + j).getType())
                                        {
                                            case "Simploxide":
                                                player1.storeOre(map.mineOre(xLocation2 + i, yLocation2 + j));
                                                map.removeOre(xLocation2 + i, yLocation2 + j);
                                                break;
                                            case "Augmentium":
                                            case "Minucium":
                                            case "Amplium":
                                                player1.storeReactor(map.mineOre(xLocation2 + i, yLocation2 + j));
                                                map.removeOre(xLocation2 + i, yLocation2 + j);
                                                break;
                                            case "Shiftoxide":
                                                int adjustment = 0;
                                                if (i == 0 && j == 0)
                                                {
                                                    try
                                                    {
                                                        int shift = ((Shiftoxide) map.getMapPosition(xLocation2 + i, yLocation2 + i)).getDirection();
                                                        map.shiftOre(((Shiftoxide) map.getMapPosition(xLocation2 + i, yLocation2 + i)), xLocation2 + i);
                                                        adjustment = (shift == 0) ? -1 : 1;
                                                    }
                                                    catch (OffBoardException e)
                                                    {
                                                        System.out.printf("\n* On Row %d, %s\n", xLocation2 + i, e.toString());
                                                    }
                                                }
                                                map.removeOre(xLocation2 + i, yLocation2 + j + adjustment);
                                                break;
                                            case "Redoxine":
                                                if (i == 0 && j == 0)
                                                {
                                                    map.diminishOre(xLocation2 + i, yLocation2 + i);
                                                }
                                                map.removeOre(xLocation2 + i, yLocation2 + j);
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    map.showOre(xLocation1, yLocation1, xLocation2, yLocation2);

                    switch (map.mineOre(xLocation1, yLocation1).getType())
                    {
                        case "Simploxide":
                            player1.storeOre(map.mineOre(xLocation1,yLocation1));
                            player1.storeOre(map.mineOre(xLocation2,yLocation2));
                            break;
                        case "Augmentium":
                        case "Minucium":
                        case "Amplium":
                            player1.storeReactor(map.mineOre(xLocation1,yLocation1));
                            player1.storeReactor(map.mineOre(xLocation2,yLocation2));
                            break;
                        case "Shiftoxide":
                            try
                            {
                                int adjustment = ((Shiftoxide) map.getMapPosition(xLocation1,yLocation1)).getDirection();
                                map.shiftOre(((Shiftoxide) map.getMapPosition(xLocation1, yLocation1)), xLocation1);
                                yLocation1 = (adjustment == 0) ? yLocation1-1 : yLocation1+1;
                            }
                            catch (OffBoardException e)
                            {
                                System.out.printf("\n* On Row %d, %s\n", xLocation1, e.toString());
                            }

                            try
                            {
                                int adjustment = ((Shiftoxide) map.getMapPosition(xLocation2,yLocation2)).getDirection();
                                map.shiftOre(((Shiftoxide) map.getMapPosition(xLocation2, yLocation2)), xLocation2);
                                yLocation2 = (adjustment == 0) ? yLocation2-1 : yLocation2+1;
                            }
                            catch (OffBoardException e)
                            {
                                System.out.printf("\n* On Row %d, %s\n", xLocation2, e.toString());
                            }
                        case "Redoxine":
                            map.diminishOre(xLocation1,yLocation1);
                            map.diminishOre(xLocation2,yLocation2);
                            break;
                    }

                    map.removeOre(xLocation1,yLocation1);
                    map.removeOre(xLocation2,yLocation2);
                }
            }
            else
            {
                map.showOre(xLocation1,yLocation1,xLocation2,yLocation2);
            }

            System.out.printf("\n%s, do you want to end the game?\nIf yes, type [-1].\n", player1.getName());
            int exit = scan.nextInt();
            // If user wishes to end the game, they will receive their current score
            if (exit == -1)
            {
                map.setSimploxideCount(0);
            }
        }

        System.out.printf("\n%s, your final score is: %d\n", player1.getName(), player1.processOre());
        System.out.print("Thank you for playing!\n");
    } // main
} // MemoryMine
