import java.util.Random;

public class Redoxine extends Ore
{
    private boolean isStable; // Determines the stability of the Redoxine ore

    /** Getter for the isStable field */
    public boolean getStable()
    {
        return isStable;
    } // getStable

    /** Setter for the isStable field */
    public void setStable(boolean stable)
    {
        isStable = stable;
    } // setStable

    /** Constructor used to initialize all data members.
     * Note: symbol  for  Redoxine  is  ‘R’; the  stability of  the  ore is
     * a random boolean value. */
    public Redoxine()
    {
        setType("Redoxine");
        setSymbol('R');

        Random rand = new Random();
        setStable(rand.nextBoolean());
    } // Redoxine
} // Redoxine
