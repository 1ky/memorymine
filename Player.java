import java.util.ArrayList;

public class Player
{
    private String name; // holds the player's name
    private int score; // holds the player's score
    private ArrayList<Ore> simploxideStore; // stores simploxide ore mined
    private ArrayList<Ore> reactorStore; // stores reaction agents mined

    /** Getter for the name field */
    public String getName() {
        return name;
    } // getName

    /** Setter for the name field */
    public void setName(String name)
    {
        this.name = name;
    } // setName

    /** Getter for the score field */
    public int getScore()
    {
        return score;
    }

    /** Setter for the score field */
    public void setScore(int score)
    {
        this.score = score;
    }

    /** Constructor for the Player Class */
    public Player(String pname)
    {
        setName(pname);
        setScore(0);
        simploxideStore = new ArrayList<>();
        reactorStore = new ArrayList<>();
    } // Player

    /** Method to store Simploxide in simploxideStore ArrayList */
    void storeOre(Ore ore)
    {
        if (ore instanceof Simploxide)
        {
            simploxideStore.add(ore);
        }
    } // storeOre

    /** Method to store reaction agents in reactorStore ArrayList */
    void storeReactor(Ore reaction)
    {
        if (reaction instanceof ReactionAgent)
        {
            reactorStore.add(reaction);
        }
    } // storeReactor

    /** Processes the ore which the player has collected in the game and returns the final score */
    public int processOre()
    {
        int total = 0;
        for (Ore o : simploxideStore)
        {
            total += ((Simploxide) o).getValue();

        }

        Simploxide simpTotal = new Simploxide();
        simpTotal.setValue(total);

        for (Ore r : reactorStore)
        {
            if (r instanceof ReactionAgent)
            {
                if (r instanceof Augmentium)
                {
                    ((Augmentium) r).oreReaction(simpTotal);
                }
                else if (r instanceof Minucium)
                {
                    ((Minucium) r).oreReaction(simpTotal);
                }
                else if (r instanceof Amplium)
                {
                    ((Amplium) r).oreReaction(simpTotal);
                }
            }
        }

        setScore(simpTotal.getValue());
        return getScore();
    } // processOre
} // Player
