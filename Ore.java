public class Ore
{
    protected String type; // stores the type of the ore
    protected char symbol; // stores a character representation of the ore

    /** Getter for the type field */
    public String getType()
    {
        return type;
    } // getType

    /** Setter for the symbol field */
    public void setSymbol(char symbol)
    {
        this.symbol = symbol;
    } // setSymbol

    /** Getter for the symbol field */
    public char getSymbol()
    {
        return symbol;
    } // getSymbol

    /** Setter for the type field */
    public void setType(String type)
    {
        this.type = type;
    } // setType

    /** Constructor  used  to  initialize  data  members  to values“Ore”  and  ‘O’ respectively. */
    public Ore()
    {
        setType("Ore");
        setSymbol('O');
    } // Ore
} // Ore
