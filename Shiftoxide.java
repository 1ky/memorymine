import java.util.Random;

public class Shiftoxide extends Ore
{
    private int direction; // Determines Which direction row will shift in

    /** Getter for direction field */
    public int getDirection()
    {
        return direction;
    } // getDirection

    /** Setter for direction field */
    public void setDirection(int direction)
    {
        this.direction = direction;
    } // setDirection

    /** Constructor used to initialize all data members.
     * Note: symbol  for  Shiftoxide  is  ‘H’; the  direction of  the  ore is
     * a random number in the range 0-1. */
    public Shiftoxide()
    {
        setType("Shiftoxide");
        setSymbol('H');

        Random rand = new Random();
        setDirection(rand.nextInt(2));
    } // Shiftoxide
} // Shiftoxide
